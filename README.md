# twofactor_duo
Forked from: https://github.com/ChristophWurst/twofactor_duo

Experimental Duo two-factor auth provider, updated for Nextcloud 14+ (tested on NC 15.0.7 currently)

## Installation
Download latest [release](https://gitlab.com/agentdr8/twofactor_duo/releases) archive and extract to your `nextcloud_install_dir/apps` (ex: /var/www/nextcloud/apps)

Enable with occ:
```
cd /var/www/nextcloud
sudo -u www-data php occ app:enable twofactor_duo
```

## Configuration
Add your duo configuration to your Nextcloud's `config/config.php` file:
```
'twofactor_duo' => [
    'IKEY' => 'xxx',
    'SKEY' => 'yyy',
    'HOST' => 'zzz',
    'AKEY' => '123',
  ],
```
## Nextcloud server patch
```
Should no longer need to patch Nextcloud files >= NC 14
```

## Working app example
After initial login screen, if twofactor_duo app is installed/enabled, you should see this:

![NC Duo example](nc_duo.PNG)